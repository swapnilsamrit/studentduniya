<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name' => 'Superadmin',
                'slug' => 'superadmin',
                'permissions' => json_encode([
                    'user-list' => true,
                    'user-add' => true,
                    'user-edit' => true,
                    'user-delete' => true,
                    'role-list' => true,
                    'role-add' => true,
                    'role-edit' => true,
                    'role-delete' => true,
                    'role-permissions' => true,
                    'universities-list' => true,
                    'universities-add' => true,
                    'universities-edit' => true,
                    'universities-delete' => true,
                    'courses-list' => true,
                    'courses-add' => true,
                    'courses-edit' => true,
                    'courses-delete' => true,
                    'questions-list' => true,
                    'questions-add' => true,
                    'questions-edit' => true,
                    'questions-delete' => true,
                    'contacts-list' => true,
                    'subscribers-list' => true,
                ]),
            ],
            [
                'name' => 'Tuition',
                'slug' => 'tuition',
                'permissions' => json_encode([

                ]),
            ]
        ]);
    }
}
