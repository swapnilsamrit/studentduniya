<?php

use Illuminate\Database\Seeder;

class ModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            [
                'name' => 'System Setting',
                'parent' => 0,
            ],
            [
                'name' => 'User',
                'parent' => 1,
            ],
            [
                'name' => 'Role',
                'parent' => 1,
            ],
            [
                'name' => 'Master',
                'parent' => 0,
            ],
            [
                'name' => 'University',
                'parent' => 4,
            ],
            [
                'name' => 'Course',
                'parent' => 4,
            ],
            [
                'name' => 'Question',
                'parent' => 4,
            ],
            [
                'name' => 'Enquiries',
                'parent' => 0,
            ],
            [
                'name' => 'Contacts',
                'parent' => 8,
            ],
            [
                'name' => 'Subscribers',
                'parent' => 8,
            ],
        ]);
    }
}
