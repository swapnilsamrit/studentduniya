<?php

use Illuminate\Database\Seeder;

class MethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('methods')->insert([
            [
                'module_id' => 2,
                'route_name' => 'user-list',
                'published' => true,
            ],
            [
                'module_id' => 2,
                'route_name' => 'user-add',
                'published' => true,
            ],
            [
                'module_id' => 2,
                'route_name' => 'user-edit',
                'published' => true,
            ],
            [
                'module_id' => 2,
                'route_name' => 'user-delete',
                'published' => false,
            ],
            [
                'module_id' => 3,
                'route_name' => 'role-list',
                'published' => true,
            ],
            [
                'module_id' => 3,
                'route_name' => 'role-add',
                'published' => true,
            ],
            [
                'module_id' => 3,
                'route_name' => 'role-edit',
                'published' => true,
            ],
            [
                'module_id' => 3,
                'route_name' => 'role-delete',
                'published' => false,
            ],
            [
                'module_id' => 3,
                'route_name' => 'role-permissions',
                'published' => false,
            ],
            [
                'module_id' => 5,
                'route_name' => 'universities-list',
                'published' => true,
            ],
            [
                'module_id' => 5,
                'route_name' => 'universities-add',
                'published' => true,
            ],
            [
                'module_id' => 5,
                'route_name' => 'universities-edit',
                'published' => true,
            ],
            [
                'module_id' => 5,
                'route_name' => 'universities-delete',
                'published' => false,
            ],
            [
                'module_id' => 6,
                'route_name' => 'courses-list',
                'published' => true,
            ],
            [
                'module_id' => 6,
                'route_name' => 'courses-add',
                'published' => true,
            ],
            [
                'module_id' => 6,
                'route_name' => 'courses-edit',
                'published' => true,
            ],
            [
                'module_id' => 6,
                'route_name' => 'courses-delete',
                'published' => false,
            ],
            [
                'module_id' => 7,
                'route_name' => 'questions-list',
                'published' => true,
            ],
            [
                'module_id' => 7,
                'route_name' => 'questions-add',
                'published' => true,
            ],
            [
                'module_id' => 7,
                'route_name' => 'questions-edit',
                'published' => true,
            ],
            [
                'module_id' => 7,
                'route_name' => 'questions-delete',
                'published' => false,
            ],
            [
                'module_id' => 8,
                'route_name' => 'contacts-list',
                'published' => true,
            ],
            [
                'module_id' => 10,
                'route_name' => 'subscribers-list',
                'published' => true,
            ],
        ]);
    }
}
