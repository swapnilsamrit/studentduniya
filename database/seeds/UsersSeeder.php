<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Admin User',
                'email' => 'admin@gmail.com',
                'mobile' => '9561471093',
                'password' => Hash::make('admin@123456'),
                'is_active' => true,
            ],
            [
                'name' => 'Tuition User',
                'email' => 'tuition@gmail.com',
                'mobile' => '9028422516',
                'password' => Hash::make('tuition@123456'),
                'is_active' => true,
            ]
        ]);
    }
}
