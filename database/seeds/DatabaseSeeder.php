<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call([
            UsersSeeder::class,
            RolesSeeder::class,
            RoleUserSeeder::class,
            MethodsSeeder::class,
            ModulesSeeder::class,
        ]);
    }
}
