@extends('layouts.dash')

@section('content')
<?php use App\Http\Controllers\Controller; ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Course</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Edit Course</li>
        </ol>
    </section>
    @php
        $reqlabel = '<sup class="text-danger">*</sup>';
        $CourseId = !empty($course->semester) ? $course->semester : NULL;
    @endphp
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Courses</h3>
                <div class="box-tools">
                    <a href="{{ route('courses-list') }}"><button class="btn btn-success btn-sm btn-flat"> Back</button></a>
                </div>
            </div>
            <form role="form" method="post" action="{{ route('courses-edit', ['course' => Controller::my_simple_crypt($course->id, 'e')]) }}">
                @csrf
                <div class="box-body">
                    <div class="form-group col-md-4">
                        <label>Select University</label>
                        <select name="university_id" id="" class="form-control select2" style="width: 100%;">
                            <option value="">Select University</option>
                            @foreach($universities as $id => $university)
                                <option value="{{ $university->id }}" @if(old('university_id', $university->id) == $course->university_id) selected @endif>{{ $university->name }}</option>
                            @endforeach    
                        </select>
                        @if ($errors->has('university_id'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('university_id') }}</small>
                            </span>
                        @endif
                    </div>
                    <!-- Roman generator function -->
                    @php
                        function integerToRoman($integer)
                        {
                            $integer = intval($integer);
                            $result = '';
                            
                            $lookup = array('M' => 1000,
                            'CM' => 900,
                            'D' => 500,
                            'CD' => 400,
                            'C' => 100,
                            'XC' => 90,
                            'L' => 50,
                            'XL' => 40,
                            'X' => 10,
                            'IX' => 9,
                            'V' => 5,
                            'IV' => 4,
                            'I' => 1);
                            
                            foreach($lookup as $roman => $value){
                                $matches = intval($integer/$value);    
                                $result .= str_repeat($roman,$matches);
                                $integer = $integer % $value;
                            }
                            return $result;
                        }
                    @endphp
                    <div class="form-group col-md-4">
                        <label>Select Semester</label>
                        <select name="semester" id="" class="form-control select2" style="width: 100%;">
                            <option value="">Select Semester</option>
                            @for($i = 1; $i <= 10; $i++)
                                <option @if(old('semester', integerToRoman($i)) == $course->semester) selected @endif>Semester - @php echo integerToRoman($i) @endphp</option>
                            @endfor
                        </select>
                        @if ($errors->has('semester'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('semester') }}</small>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputName">Course Name</label>
                        <input type="text" class="form-control" id="exampleInputName" name="name" value="{{ old('name', $course->name) }}" placeholder="Enter course name">
                        @if ($errors->has('name'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('name') }}</small>
                            </span>
                        @endif 
                    </div>
                </div>
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <button type="reset" class="btn btn-default">Cancel</button>
                </div>
            </form>
        </div>
    </section>
</div>
@endsection
