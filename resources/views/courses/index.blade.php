@extends('layouts.dash')

@section('content')
<?php use App\Http\Controllers\Controller; ?>
<div class="content-wrapper">
    @if(Session::has('message'))
        <div id="flashDivId" class="callout callout-{{ Session::get('alert-class', 'info') }}" onclick="$(this).slideUp();" style="border-radius:unset;">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ Session::get('message') }}</strong>
        </div>
    @endif
    <section class="content-header">
        <h1>Course</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Courses List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Courses List</h3>
                        <div class="box-tools">
                            <a href="{{ route('courses-add') }}"><button class="btn btn-success btn-sm btn-flat"> ADD</button></a>
                        </div>
                    </div>
                    <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th style="width: 10px">S.No</th>
                                <th>Name</th>
                                <th>University</th>
                                <th style="width: 10%">Action</th>
                            </tr>
                            @foreach($allcourses as $course)
                            @php $encyId = Controller::my_simple_crypt($course->id, 'e') @endphp
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $course->name }}  {{ $course->semester }}</td>
                                    <td>{{ $course->university->name }}</td>
                                    <td>
                                        @can('courses-edit')
                                        <a href="{{ route('courses-edit', ['course' => $encyId]) }}"><button class="btn btn-primary btn-xs btn-flat"> Edit</button></a>
                                        @endcan
                                        @can('courses-delete')
                                        <a href="{{ url('/dashboard/'.$course->id.'/courses') }}" onclick="return confirm('Are you sure?')" title="Delete"><button class="btn btn-danger btn-xs btn-flat"> Delete</button></a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach   
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
