@extends('layouts.dash')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Course</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Add Course</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Add Courses</h3>
                <div class="box-tools">
                    <a href="{{ route('courses-list') }}"><button class="btn btn-success btn-sm btn-flat"> Back</button></a>
                </div>
            </div>
            <form role="form" method="post" action="{{ route('courses-add') }}">
                @csrf
                <div class="box-body">
                    <div class="form-group col-md-4">
                        <label>Select University</label>
                        <select name="university_id" id="universityId" class="form-control select2" style="width: 100%;">
                            <option value="" id="universityOptionId">Select University</option>
                            @foreach($universities as $university)
                                <option value="{{ $university->id }}">{{ $university->name }}</option>
                            @endforeach    
                        </select>
                        @if ($errors->has('university_id'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('university_id') }}</small>
                            </span>
                        @endif
                    </div>
                    <!-- Roman generator function -->
                    @php
                        function integerToRoman($integer)
                        {
                            $integer = intval($integer);
                            $result = '';
                            
                            $lookup = array('M' => 1000,
                            'CM' => 900,
                            'D' => 500,
                            'CD' => 400,
                            'C' => 100,
                            'XC' => 90,
                            'L' => 50,
                            'XL' => 40,
                            'X' => 10,
                            'IX' => 9,
                            'V' => 5,
                            'IV' => 4,
                            'I' => 1);
                            
                            foreach($lookup as $roman => $value){
                                $matches = intval($integer/$value);    
                                $result .= str_repeat($roman,$matches);
                                $integer = $integer % $value;
                            }
                            return $result;
                        }
                    @endphp
                    <div class="form-group col-md-4">
                        <label>Select Semester</label>
                        <select name="semester" id="semesterId" class="form-control select2" style="width: 100%;" disabled>
                            <option value="">Select Semester</option>
                            @for($i = 1; $i <= 10; $i++)
                                <option value="Semester <?php echo integerToRoman($i) ?>">Semester - <?php echo integerToRoman($i) ?></option>
                            @endfor
                        </select>
                        @if ($errors->has('semester'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('semester') }}</small>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputName">Course Name</label>
                        <input type="text" class="form-control" id="exampleInputName" name="name" placeholder="Enter course name">
                        @if ($errors->has('name'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('name') }}</small>
                            </span>
                        @endif 
                    </div>
                </div>
                <div class="box-footer text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-default">Cancel</button>
                </div>
            </form>
        </div>
    </section>
</div>
@endsection
