@extends('layouts.dash')

@section('content')
<?php use App\Http\Controllers\Controller; ?>
<div class="content-wrapper">
    @if(Session::has('message'))
        <div id="flashDivId" class="callout callout-{{ Session::get('alert-class', 'info') }}" onclick="$(this).slideUp();" style="border-radius:unset;">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ Session::get('message') }}</strong>
        </div>
    @endif
    <section class="content-header">
        <h1>University</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Universities List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Universities List</h3>
                        <div class="box-tools">
                            <a href="{{ route('universities-add') }}"><button class="btn btn-success btn-sm btn-flat"> ADD</button></a>
                        </div>
                    </div>
                    <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th style="width: 10px">S.No</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                            @if(!empty($alluniversities) && count($alluniversities) > 0)
                                @foreach($alluniversities as $id => $university)
                                @php $encyId = Controller::my_simple_crypt($university->id, 'e') @endphp
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $university->name }}</td>
                                        <td>{{ $university->description }}</td>
                                        <td>
                                            @can('universities-edit')
                                            <a href="{{ route('universities-edit', ['universities' => $encyId]) }}"><button class="btn btn-primary btn-xs btn-flat"> Edit</button></a>
                                            @endcan
                                            @can('universities-delete')
                                            <a href="{{ url('/dashboard/'.$university->id.'/universities')}}" onclick="return confirm('Are you sure?')" title="Delete"><button class="btn btn-danger btn-xs btn-flat"> Delete</button></a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach    
                            @endif
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
