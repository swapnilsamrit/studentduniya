@extends('layouts.dash')

@section('content')
<?php use App\Http\Controllers\Controller; ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>University</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Edit University</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Universities</h3>
            </div>
            <form role="form" method="post" action="{{ route('universities-edit', ['universities' => Controller::my_simple_crypt($university->id, 'e')]) }}">
                @csrf
                <div class="box-body">
                    <div class="form-group col-md-4">
                        <label for="exampleInputName">University Name</label>
                        <input type="text" class="form-control" id="nameId" placeholder="Enter university name" name="name" value="{{ old('name', $university->name) }}" autofocus required>
                        @if ($errors->has('name'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('name') }}</small>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group col-md-4">
                        <label for="exampleInputName">Description</label>
                        <textarea name="description" class="form-control" placeholder="Enter description">{{ old('description', $university->description) }}</textarea>
                        @if ($errors->has('description'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('description') }}</small>
                            </span>
                        @endif
                    </div> 
                </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                    </div>
            </form>
        </div>
    </section>
</div>
@endsection
