@extends('layouts.dash')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>University</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Add University</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Add Universities</h3>
            </div>
            <form role="form" method="post" action="{{ route('universities-add') }}">
                @csrf
                <div class="box-body">
                    <div class="form-group col-md-4">
                        <label for="exampleInputName">University Name</label>
                        <input type="text" class="form-control" placeholder="Enter university name" name="name" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('name') }}</small>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group col-md-4">
                        <label for="exampleInputName">Description</label>
                        <textarea name="description" class="form-control" placeholder="Enter description"></textarea>
                    </div> 
                </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                    </div>
            </form>
        </div>
    </section>
</div>
@endsection
