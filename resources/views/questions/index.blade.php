@extends('layouts.dash')

@section('content')
<?php use App\Http\Controllers\Controller; ?>
<div class="content-wrapper">
    @if(Session::has('message'))
        <div id="flashDivId" class="callout callout-{{ Session::get('alert-class', 'info') }}" onclick="$(this).slideUp();" style="border-radius:unset;">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ Session::get('message') }}</strong>
        </div>
    @endif
    <section class="content-header">
        <h1>Question</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Questions List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Questions List</h3>
                        <div class="box-tools">
                            <a href="{{ route('questions-add') }}"><button class="btn btn-success btn-sm btn-flat"> ADD</button></a>
                        </div>
                    </div>
                    <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th style="width: 10px">S.No</th>
                                <th>University</th>
                                <th>Course</th>
                                <th>Paper Year</th>
                                <th>Season</th>
                                <th>Subject Name</th>
                                <th>Action</th>
                            </tr>
                            @if(!empty($allquestions) && count($allquestions) > 0)
                                @foreach($allquestions as $id => $question)
                                @php $encyId = Controller::my_simple_crypt($question->id, 'e') @endphp
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $question->university->name }}</td>
                                        <td>{{ $question->courses->name }} {{ $question->courses->semester }}</td>
                                        <td>{{ $question->year }}</td>
                                        <td>{{ $question->session }}</td>
                                        <td>{{ $question->subject_name }}</td>
                                        <td>
                                            @can('questions-delete')
                                            <a href="{{ url('/dashboard/'.$question->id.'/questions') }}" onclick="return confirm('Are you sure?')" title="Delete"><button class="btn btn-danger btn-xs btn-flat"> Delete</button></a>
                                            @endcan
                                        </td>
                                    </tr>
                                @endforeach    
                            @endif
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
