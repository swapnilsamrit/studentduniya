@extends('layouts.dash')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Question</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Add Question</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Add Question</h3>
                <div class="box-tools">
                    <a href="{{ route('questions-list') }}"><button class="btn btn-success btn-sm btn-flat"> Back</button></a>
                </div>
            </div>
            <form role="form" method="post" action="{{ route('questions-add') }}" enctype="multipart/form-data">
                @csrf
                <div class="box-body">
                    <div class="form-group col-md-4">
                        <label>Select University</label>
                        <select id="selectUniversityId" name="university_id" class="form-control select2" style="width: 100%;">
                            <option value="">Select University</option>
                            @foreach($universities as $university)
                                <option value="{{ $university->id }}">{{ $university->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('university_id'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('university_id') }}</small>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <label>Select Course</label>
                        <select id="selectCourseId" name="course_id" class="form-control select2" style="width: 100%;" disabled>
                            <option>Select Course</option>
                            @foreach($courses as $course)
                                <option value="{{ $course->id }}">{{ $course->name }} {{ $course->semester }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('course_id'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('course_id') }}</small>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-4">
                        <label>Select Year</label>
                        <select id="selectYearId" name="year" class="form-control select2" style="width: 100%;" disabled>
                            <option>Select Year</option>
                            @for ($i = 2010; $i <= date("Y"); $i++)
                                <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                        </select>
                        @if ($errors->has('year'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('year') }}</small>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group col-md-4">
                        <label>Select Session</label>
                        <select id="selectSessionId" name="session" class="form-control select2" style="width: 100%;" disabled>
                            <option>Select Session</option>
                            <option value="summer">Summer</option>
                            <option value="winter">Winter</option>
                        </select>
                        @if ($errors->has('session'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('session') }}</small>
                            </span>
                        @endif 
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputName">Subject Name</label>
                        <input type="text" name="subject_name" class="form-control" id="exampleInputName" name="name" placeholder="Enter subject name">
                        @if ($errors->has('subject_name'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('subject_name') }}</small>
                            </span>
                        @endif 
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="exampleInputFile">Upload Question Paper</label>
                            <input type="file" name="file_url" class="form-control" id="exampleInputFile">
                        </div>
                        @if ($errors->has('file_url'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('file_url') }}</small>
                            </span>
                        @endif 
                    </div>
                </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                    </div>
            </form>
        </div>
    </section>
</div>
@endsection
