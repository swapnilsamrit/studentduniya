<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <title>STUDENT-DUNIYA</title>
    <link href="{{ asset('public/website/css/bootstrap.min.cs') }}s" rel="stylesheet">
    <link href="{{ asset('public/website/css/jquery.bxslider.css') }}" rel="stylesheet" >
    <link href="{{ asset('public/website/css/base.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('public/website/css/style.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
	@include('webelements.header')
    @yield('content')
    @include('webelements.footer')

    <script src="{{ asset('public/website/js/jquery.js') }}"></script>
    <script src="{{ asset('public/website/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/website/js/jquery.bxslider.min_optimized.js') }}"></script>
    <script src="{{ asset('public/website/js/typed.js') }}"></script>
    <script src="{{ asset('public/website/js/sdcommon.js') }}"></script>
    <script>
    //new update slider
      $(document).ready(function(){
      	if(window.innerWidth <= 640){
        var count = 1;
      }else{
        var count = 3;
      }
        $('.slider5').bxSlider({
          slideWidth:510,
          minSlides: count,
          maxSlides: count,
          moveSlides: 1,
          slideMargin:10,
          auto:true,
          autoControls: true,  
          pager : false,
          nextText: '<img src="img/right-arrow.png" height="40" width="80"/>',
          prevText: '<img src="img/left-arrow.png" height="40" width="80"/>'
        });
      });
      $(document).ready(function(){ 
        $('.bx-viewport').css('min-height','140px');
      });

      //typing effect on header image
      $(function(){
          $(".typing-text").typed({
            strings: ["THE NEEDS OF STUDENTS", "THEIR DIFFICULTIES"],
            typeSpeed: 100,
             backDelay: 1200,
            loop: true,
          });
      });

      	$(document).ready(function(){ 
            $('html, body').animate({scrollTop: 0}, 1000);
        });

       $(document).ready(function(){ 
          	$(".down-arrow").click(function() {
    			$('html,body').animate({
        		scrollTop: $("#info-form").offset().top},
        		'slow');
			});
        });
       //bottom-shadow to navigation
       $(window).scroll(function() {     
    		var scroll = $(window).scrollTop();
    		if (scroll > 0) {
        		$("#header").addClass("active");
    		}
    		else {
        		$("#header").removeClass("active");
   			}
		});
    </script>
</body>
</html>