<footer class="main-footer">
    <div class="pull-right hidden-xs"></div>
    <strong>Copyright &copy; 2019-2020 <a href="#">{{ config('app.name', 'Laravel') }}</a>.</strong> All rights reserved.
</footer>