<aside class="main-sidebar">
    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset('public/dashboard/dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li>
            <a href="{{ route('home') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        @php
            $activeSystemSetClass = '';
            $SystemSetArr = ['user-list','role-list','user-add','role-add','user-edit','role-edit'];
            if(in_array(Route::currentRouteName(), $SystemSetArr)){
                $activeSystemSetClass = 'active menu-open';
            }
        @endphp
        @if(Gate::check('user-list') || Gate::check('role-list'))
        <li class="treeview {{ $activeSystemSetClass }}">
            <a href="#">
                <i class="fa fa-gear"></i> <span>System Setting</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                @can('user-list')
                <li class="@if(in_array(Route::currentRouteName(),['user-list','user-add','user-edit'])) active @endif"><a href="{{ route('user-list') }}"><i class="fa fa-circle-o"></i> User</a></li>
                @endcan
                @can('role-list')
                <li class="@if(in_array(Route::currentRouteName(),['role-list','role-add','role-edit'])) active @endif"><a href="{{ route('role-list') }}"><i class="fa fa-circle-o"></i> Role</a></li>
                @endcan
            </ul>
        </li>
        @endif
        @php
            $activeSystemSetClass = '';
            $SystemSetArr = ['universities-list','courses-list','questions-list'];
            if(in_array(Route::currentRouteName(), $SystemSetArr)){
                $activeSystemSetClass = 'active menu-open';
            }
        @endphp
        @if(Gate::check('universities-list') || Gate::check('courses-list') || Gate::check('questions-list'))
        <li class="treeview {{ $activeSystemSetClass }}">
            <a href="#">
                <i class="fa fa-laptop"></i> <span>Master</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                @can('universities-list')
                <li class="@if(in_array(Route::currentRouteName(),['universities-list','universities-add','universities-edit'])) active @endif"><a href="{{ route('universities-list') }}"><i class="fa fa-circle-o"></i> University</a></li>
                @endcan
                @can('courses-list')
                <li class="@if(in_array(Route::currentRouteName(),['courses-list','courses-add','courses-edit'])) active @endif"><a href="{{ route('courses-list') }}"><i class="fa fa-circle-o"></i> Course</a></li>
                @endcan
                @can('questions-list')
                <li class="@if(in_array(Route::currentRouteName(),['questions-list','questions-add','questions-edit'])) active @endif"><a href="{{ route('questions-list') }}"><i class="fa fa-circle-o"></i> Question</a></li>
                @endcan
            </ul>
        </li>
        @endif
        @php
            $activeSystemSetClass = '';
            $SystemSetArr = ['contacts-list','subscribers-list'];
            if(in_array(Route::currentRouteName(), $SystemSetArr)){
                $activeSystemSetClass = 'active menu-open';
            }
        @endphp
        @if(Gate::check('contacts-list') || Gate::check('subscribers-list'))
        <li class="treeview {{ $activeSystemSetClass }}">
            <a href="#">
                <i class="fa fa-laptop"></i> <span>Enquiry</span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
            </a>
            <ul class="treeview-menu">
                @can('contacts-list')
                <li class="@if(in_array(Route::currentRouteName(),['contacts-list'])) active @endif"><a href="{{ route('contacts-list') }}"><i class="fa fa-circle-o"></i> Website Enquiry</a></li>
                @endcan
                @can('subscribers-list')
                <li class="@if(in_array(Route::currentRouteName(),['subscribers-list'])) active @endif"><a href="{{ route('subscribers-list') }}"><i class="fa fa-circle-o"></i> Subscribers</a></li>
                @endcan
            </ul>
        </li>
        @endif
    </ul>
    </section>
</aside>