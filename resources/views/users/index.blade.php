@extends('layouts.dash')

@section('content')
<?php use App\Http\Controllers\Controller; ?>
<div class="content-wrapper">
    @if(Session::has('message'))
        <div id="flashDivId" class="callout callout-{{ Session::get('alert-class', 'info') }}" onclick="$(this).slideUp();" style="border-radius:unset;">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ Session::get('message') }}</strong>
        </div>
    @endif
    <section class="content-header">
        <h1>User</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Users List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Users List</h3>
                        <div class="box-tools">
                            <a href="{{ route('user-add') }}"><button class="btn btn-success btn-sm btn-flat"> ADD</button></a>
                        </div>
                    </div>
                    <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <th style="width: 10px">S.No</th>
                                <th>Name</th>
                                <th>Email/Login Id</th>
                                <th>Mobile</th>
                                <th>Role</th>
                                <th>Status</th>
                                <th width="12%">Action</th>
                            </tr>
                            @foreach($userlist as $user)
                                @php
                                    $encyId = Controller::my_simple_crypt($user->id, 'e');
                                    $roleName = '';
                                    if(!empty($user->roles) && count($user->roles) > 0){
                                        $roleName = $user->roles[0]->name;
                                    }
                                @endphp
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->mobile }}</td>
                                    <td>{{ $roleName }}</td>
                                    <td>
                                        <span class="btn btn-xs bg-green waves-effect">
                                            {{ ($user->is_active) ? "Active" : "Inactive" }}
                                        </span>
                                    </td>
                                    <td>
                                        @can('user-edit')
                                        <a href="{{ route('user-edit', ['user' => $encyId]) }}"><button class="btn btn-primary btn-xs"><i class="fa fa-edit"></i> Edit</button></a>
                                        @endcan
                                        @can('user-delete')
                                        <a href="{{ url('/dashboard/'.$user->id.'/users')}}" onclick="return confirm('Are you sure?')" title="Delete"><button class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i> Delete</button></a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
