@extends('layouts.dash')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>User</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add Users</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Add Users</h3>
            </div>
            <form role="form" method="post" action="{{ route('register') }}">
                @csrf
                <div class="box-body">
                    <div class="form-group col-md-4">
                        <label for="exampleInputName">Name</label>
                        <input type="text" class="form-control" id="exampleInputName" name="name" placeholder="Enter name">
                        @if ($errors->has('name'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('name') }}</small>
                            </span>
                        @endif
                    </div>                                        
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail">Email</label>
                        <input type="text" class="form-control" id="exampleInputEmail" name="email" placeholder="Enter email">
                        @if ($errors->has('email'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('email') }}</small>
                            </span>
                        @endif
                    </div>                                        
                    <div class="form-group col-md-4">
                        <label for="exampleInputMobile">Mobile</label>
                        <input type="text" class="form-control" id="exampleInputMobile" name="mobile" placeholder="Enter mobile">
                        @if ($errors->has('mobile'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('mobile') }}</small>
                            </span>
                        @endif
                    </div>                                        
                    <div class="form-group col-md-4">
                        <label for="exampleInputPassword">Password</label>
                        <input type="password" class="form-control" id="exampleInputPassword" name="password" placeholder="Enter password">
                        @if ($errors->has('password'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('password') }}</small>
                            </span>
                        @endif
                    </div>                  
                    <div class="form-group col-md-4">
                        <label for="exampleInputCnfPassword">Confirm Password</label>
                        <input type="password" class="form-control" id="exampleInputCnfPassword" name="password_confirmation" placeholder="Enter confirm password">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputRole">Select Role</label>
                        <select name="role_id" id="roleId" class="form-control select2" style="width: 100%;">
                            <option value="">Select role</option>
                            @foreach($roles as $id => $name)
                                <option value="{{ $id }}" @if(old('role_id') == $id) Selected @endif>{{ $name }}</option>
                            @endforeach    
                        </select>
                        @if ($errors->has('role_id'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('role_id') }}</small>
                            </span>
                        @endif
                    </div>
                    <div class="row"></div>
                    <div class="col-md-3">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_active" checked=""> <strong>Is Active</strong>
                            </label>
                        </div>
                    </div>
                </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                    </div>
            </form>
        </div>
    </section>
</div>
@endsection
