@extends('layouts.app')

@section('content')
<header>
    <div class="container-fluid head-banner" id="head-banner">
        <div class="col-xs-12 container no-padding text-white text-center">
            <h4>WE UNDERSTAND <span class="typing-text"></span></h4>
            <h1>WE PROVIDE BEST MATERIAL <br>FOR STUDENTS</h1>
            <img class="down-arrow" src="{{ asset('public/website/img/arrow.gif') }}" alt="Down-Arrow">
        </div>
    </div>
</header>
<section class="section1 padding30">
    <div class="container-fluid">
        <div class="new-updates text-center">
            <h2 class="margin20">NEW UPDATES</h2>
            <p class="lg margin20">We are updated with new question papers and much more.....</p>
            <div class="container">
                <div class="slider5 col-sm-12 no-padding margin20">	
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container resources-provided text-center padding30">
        <h2>RESOURCES WE PROVIDE</h2>
        <p class="lg margin20">All the things you are searching for.... You will find it here...</p>
        <div class="col-sm-12 text-white">
            <div class="col-md-3 col-xs-6 no-padding col-resourse">
                <div class="resource-cont clearfix">
                    <div class="col-sm-12 no-padding syllabus resource-box">
                        <span class="icon-tool"></span>
                    </div>
                    <div class="col-sm-12 no-padding resourse-info padding15">
                        <h4>SYLLABUS</h4>
                        <p class="lg">We Provide the Syllabus of all the courses in RTMNU</p>
                        <a href="javascript:void(0);" class="btn btn-info view-btn">VIEW</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 no-padding col-resourse">
                <div class="resource-cont clearfix">
                    <div class="col-sm-12 no-padding que-paper resource-box">
                        <span class="icon-folder"></span>
                    </div>
                    <div class="col-sm-12 no-padding resourse-info padding15">
                        <h4>QUESTION PAPERS</h4>
                        <p class="lg">We Provide the bulk of back years Question Papers.</p>
                        <a href="{{ route('questionPaper') }}" class="btn btn-info view-btn">VIEW</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 no-padding col-resourse">
                <div class="resource-cont clearfix">
                    <div class="col-sm-12 no-padding ans-paper resource-box">
                        <span class="icon-question-class-note-symbol"></span>
                    </div>
                    <div class="col-sm-12 no-padding resourse-info padding15">
                        <h4>ANSWER PAPERS</h4>
                        <p class="lg">Model Answer Papers help you to solve the Question Papers.</p>
                        <a href="javascript:void(0);" class="btn btn-info view-btn">VIEW</a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 no-padding col-resourse">
                <div class="resource-cont clearfix">
                    <div class="col-sm-12 no-padding tutorial-classes resource-box">
                        <span class="icon-audience-in-presentation-of-business"></span>
                    </div>
                    <div class="col-sm-12 no-padding resourse-info padding15">
                        <h4>TUTORIAL CLASSES</h4>
                        <p class="lg">We Provide the Information of all Tutorial classes in your city.</p>
                        <a href="tution-classes.php" class="btn btn-info view-btn">VIEW</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<section>
    <div class="container-fluid testimonial no-padding text-center text-white padding30">
        <h2 class="padding30">WHAT STUDENTS ARE SAYING</h2>
        <div id="carousel-example-generic" class="carousel slide col-sm-12" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active col-sm-12">
                        <div class="container">
                            <div class="student-info">
                                <div class="student-photo">
                                    <img src="{{ asset('public/website/img/testimonial-img.png') }}" alt="testimonial-img">
                                </div>
                                <p class="xl col-xs-10 col-xs-offset-1  padding30"><q>I found your blog is awesome. I will always going to be in touch with this blog since its knowledgeable.</q></p>
                                <h4 class="col-sm-12 color-blue">PRATIK WANKHEDE</h4>
                                <h5 class="col-sm-12">Student, Wardha</h5>
                                <div class="col-sm-12 ratings">
                                    <span class="icon-star"></span>
                                    <span class="icon-star"></span>
                                    <span class="icon-star"></span>
                                    <span class="icon-star"></span>
                                    <span class="icon-star-2"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="container">
                            <div class="student-info">
                                <div class="student-photo">
                                    <img src="{{ asset('public/website/img/testimonial-img.png') }}" alt="Shubham-Falke">
                                </div>
                                <p class="xl col-xs-10 col-xs-offset-1 padding30"><q>Its a wonderful creativity and useful as a student forum.</q></p>
                                <h4 class="col-sm-12 color-blue">SHUBHAM FALKE</h4>
                                <h5 class="col-sm-12">Student, Wardha</h5>
                                <div class="col-sm-12 ratings">
                                    <span class="icon-star"></span>
                                    <span class="icon-star"></span>
                                    <span class="icon-star"></span>
                                    <span class="icon-star"></span>
                                    <span class="icon-star-2"></span>
                                </div>
                            </div>
                        </div>	
                    </div>
                    <div class="item">
                        <div class="container">
                            <div class="student-info">
                                <div class="student-photo">
                                    <img src="{{ asset('public/website/img/testimonial-img.png') }}" alt="testimonial-img">
                                </div>
                                <p class="xl col-xs-10 col-xs-offset-1 padding30"><q>Loved your blog information. Your site is one of the best I've found for solid study materials. Keep up the great work...!</q></p>
                                <h4 class="col-sm-12 color-blue">SACHIN KATAKWAR</h4>
                                <h5 class="col-sm-12">Student, Wardha</h5>
                                <div class="col-sm-12 ratings">
                                    <span class="icon-star"></span>
                                    <span class="icon-star"></span>
                                    <span class="icon-star"></span>
                                    <span class="icon-star"></span>
                                    <span class="icon-star-2"></span>
                                </div>
                            </div>
                        </div>	
                    </div>
                </div>
            <a class="left carousel-control slider-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon icon-left-arrow" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control slider-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon icon-arrows" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
<section>
    <div class="container-fluid notification-section text-center padding30">
        <h2 class="margin20">GET NOTIFIED ON EVERY UPDATE</h2>
        <p class="lg margin20">We will send you information about latest updates on website</p>
        <form class="form-inline col-xs-12 no-padding text-center">
            <div class="form-group margin20">
                <input type="email" class="form-control" placeholder="Email">
                <button type="button" class="btn btn-info subscribe">SUBSCRIBE</button>
            </div>
        </form>
    </div>
</section>
@endsection
