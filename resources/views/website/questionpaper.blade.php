@extends('layouts.app')

@section('content')
<header class="paper">
    <div class="header-banner container-fluid">
        <div class="container text-white">
            <h1 class="margin20">PAPER/ENGINEERING</h1>
            <h3 class="margin-side10 text-white"><span class="typing-text"></span></h3>
        </div>
    </div>
</header>
<!-- information form -->
<div class="container-fluid info-formsection quick-find" id="info-form">
    <div class="col-sm-12 no-padding text-center text-white no-padding">
        <h2 class="margin20">QUICK FIND</h2>
        <div class="container col-xs-12 no-margin margin20 no-padding">
            <form method="post" accept-charset="utf-8" class="form-inline col-xs-12 no-padding" action="/users/searchpaper"><div style="display:none;"><input type="hidden" name="_method" value="POST"/><input type="hidden" name="_csrfToken" autocomplete="off" value="ea3887f8a2de44f84f605f4a6a3628e3df74eca8b7710aff60167abf716ca83c150b60f53ab8a40f1987feaf55367a494eb851f7a6386cea3b78792c9a49f387"/></div>            <div class="form-group margin-side10 no-padding">
                <select class="form-control" required="required" name="paper_year">
                    <option>2010</option>
                </select>
                <select class="form-control" required="required" name="course_id">
                    <option value="B. Arch. (I.D.) Eighth Semester ">B. Arch. (I.D.) Eighth Semester </option>
                </select>
                <div class="submit"><input type="submit" class="btn btn-default btn-search" value="Search"/></div>            
            </form>            
        </div>
    </div>
</div>
</div>
    <div class="row">
        <div class="col-md-12">
            <div class="related">
                <h4 class="text-center">Question Papers</h4>
                <table cellpadding="0" cellspacing="0">
                    <div class="col-sm-4 col-xs-12">
                        <div class="paper-categ paper-subject-div">
                            <div class="paper-type text-center">
                                <h2>SOFT COMPUTING TECHNIQUES(E-III)</h2>
                                <h4>B.E. Eighth Semester (Computer Science &amp; Engineering) [CBS]</h4>
                                <h5>Winter &nbsp; <span class="icon-days type-icon"></span> 2017</h5>
                            </div>
                            <div class="paper-download-div">
                                <form method="post" accept-charset="utf-8" action="/users/download"><div style="display:none;"><input type="hidden" name="_method" value="POST"/><input type="hidden" name="_csrfToken" autocomplete="off" value="ea3887f8a2de44f84f605f4a6a3628e3df74eca8b7710aff60167abf716ca83c150b60f53ab8a40f1987feaf55367a494eb851f7a6386cea3b78792c9a49f387"/></div>                  <input type="hidden" name="id" value="file/question_paper/2018/916019SOFT COMPUTING TECHNIQUES(E-III).pdf"/>                  <div class="submit"><input type="submit" class="btn btn-info btn-sm" value="Download"/></div>                  </form>                    </div>
                            </div>
                        </div>
                    </div>                   
                </table>
            </div>
        </div>
    </div> 
@endsection