@extends('layouts.app')

@section('content')
<header class="about">
        <div class="header-banner container-fluid">
            <div class="container text-white">
                <h1 class="margin20">ABOUT US</h1>
                <h3 class="margin-side10 text-white"><span class="typing-text"></span></h3>
            </div>
        </div>
    </header>
                                    
    <section>
        <div class="about-bg">
            <div class="col-xs-12 text-justify about-info">
                <h2 class="text-center">OUR STORY FOR STUDENTDUNIYA</h2>
                <p class="sm">This was all started from our struggle for our studies. Being engineering students we all know how we search for notes of subjects at the last time of our exams but did not found proper notes and books. Not only the engineering students but all the students from SSC, HSC, BSc, BA etc. from all platforms, all struggle for proper notes and study materials. This problem is faced by all every time but nobody thinks to overcome it.
                One day we all friends were at our friend’s room and were studying. One of our friends got struck with an idea that can we gather all the study materials, question papers and all other stuff for students? Can we make a platform for students where they can find all study stuff? Here we go…… On the basis of this idea, we started our venture with website studentduniya.com</p>
                <p class="sm">
                Then why to wait? All we friends started our work of planning all the things, gathering information, all study materials, questions papers etc. But as you know 3-4 people cannot do such vast work in limited time and cannot gather all the information without any support.  Because of our good fortune, we met some other students who liked our Idea very much and agreed to support us at their extreme level.  And this way work started at different levels and different category. We all are so excited about our work because “Students are working for students”. Now at this level, we are ready with our first version of our website for students and very soon we will take it to another level. All we want from students is the support and your appreciation. Without it, we cannot do what we are thinking of doing for students..</p>
            </div>
        <div class="col-sm-12">
            <div class="col-sm-4">
                <h2 class="text-center text-primary">Core Members</h2>
                <ul class="list-group">
                    <li class="list-group-item">Abhishek Sukla</li>
                    <li class="list-group-item">Aadil Sheikh</li>
                    <li class="list-group-item">Vaibhao Patil</li>
                    <li class="list-group-item">kalyani Pardhi</li>
                    <li class="list-group-item">Ankush Ekhapure</li>
                    <li class="list-group-item">Sayyed Tabish</li>
                    <li class="list-group-item">Sumit Kamble</li>
                </ul>
            </div>
            <div class="col-sm-4">
                <h2 class="text-center text-primary">Team Members</h2>
                <ul class="list-group">
                    <li class="list-group-item">Mayur Rangari</li>
                    <li class="list-group-item">Umesh Pawar</li>
                    <li class="list-group-item"> Hrushikesh nagrale</li>
                    <li class="list-group-item">kshitija Deshmukh</li>
                    <li class="list-group-item">Jaya Bhargaw</li>
                    <li class="list-group-item">Rani Yadav</li>
                    <li class="list-group-item">Pawan Shewade</li>
                    <li class="list-group-item">Shweta Raut</li>
                    <li class="list-group-item">Deepa Yadav</li>
                    <li class="list-group-item">Komal Patil</li>
                    <li class="list-group-item">Yash Rode</li>
                    <li class="list-group-item">Tejaswini Yanurkar</li>
                    <li class="list-group-item">Shobhana Chaudhari</li>
                    <li class="list-group-item">Apurva Sakle</li>
                    <li class="list-group-item">Shehal Gaikwad</li>
                </ul>
            </div>
            <div class="col-sm-4">
                <h2 class="text-center text-primary">Supporting Team</h2>
                <ul class="list-group">
                    <li class="list-group-item">Suraj likhar</li>
                    <li class="list-group-item">Akib Sayyad</li>
                    <li class="list-group-item">Shubham wasnik</li>
                    <li class="list-group-item">Rajedra Dubey</li>
                    <li class="list-group-item">Dinesh Barai</li>
                    <li class="list-group-item">Sunny manwatkar</li>
                    <li class="list-group-item">Sushil Wadalkar</li>
                    <li class="list-group-item">Praney Dukre</li>
                    <li class="list-group-item">Shubham Shelki</li>
                </ul>
            </div>
        </div>
        </div>
        <div class="container">
        </div>
        </section>
@endsection
