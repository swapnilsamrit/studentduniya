@extends('layouts.app')

@section('content')
<header class="contact">
    	<div class="header-banner container-fluid">
    		<div class="container text-white">
    			<h1 class="margin20">CONTACT US</h1>
    			<h3 class="margin-side10 text-white"><span class="typing-text"></span></h3>
    		</div>
    	</div>
    </header>
    <section>
        <div class="contact-section container-fluid padding30">
            <div class="enquiry-details">
                <div class="container">
                    <div class="col-sm-7 col-xs-12 padding30 padding15">
                        @if(Session::has('success'))
                            <div id="flashDivId" class="callout callout-{{ Session::get('alert-class', 'info') }}" onclick="$(this).slideUp();" style="border-radius:unset;">
                                <strong>{{ Session::get('success') }}</strong>
                            </div>
                        @endif
              			<h2>ENQUIRY FORM</h2>
              			<p class="lg">Ask any of your quries regarding your Education and career. We are here to help you out. Feel free to write us.</p>
              			<div class="col-xs-12 no-padding contact-form margin20">
                			<form class="form" method="post" action="{{ route('contact') }}">
                                @csrf
                  				<div class="form-group col-xs-6  reg-form padding-left0">
                   					<input type="text" class="form-control reg-field" name="fname" placeholder="Your First Name">
                  				</div>
                  				<div class="form-group col-xs-6  reg-form">
                    				<input type="text" class="form-control reg-field" name="lname" placeholder="Your Last Name">
                  				</div>
                  				<div class="form-group col-xs-6  reg-form padding-left0">
                    				<input type="text" class="form-control reg-field" name="eduction" placeholder="Education">
                  				</div>
                  				<div class="form-group col-xs-6  reg-form">
                    				<input type="text" class="form-control reg-field" name="school" placeholder="School/College Name">
                  				</div>
                  				<div class="form-group col-xs-6  reg-form padding-left0">
                    				<input type="email" class="form-control reg-field" name="email" placeholder="Your Email">
                  				</div>
                  				<div class="form-group col-xs-6  reg-form">
                    				<input type="text" class="form-control reg-field" name="phone" placeholder="Your Phone No.">
                  				</div>
                  				<div class="form-group col-xs-12 reg-form padding-left0">
                    				<textarea class="form-control message" rows="3" name="message" placeholder="Your Message"></textarea>
                  				</div>
                  				<div class="form-group col-xs-12 text-left">
                  					<button type="submit" class="btn btn-info view-btn">Submit</button>
                  				</div>
                			</form>
              			</div>
            		</div>
            		<div class="col-sm-5 col-xs-12 padding30 padding15">
             			 <div class="col-xs-12">
                			<h2>ADDRESS</h2>
              				<div class="address">
                				<p class="xl no-margin"><span class="icon-placeholder"></span>Lahanuji Nagar</p>
                				<p class="lg">Civil line behind head post office, Wardha. 442001</p>
                				<p class="lg"><span class="icon-old-typical-phone"></span> +91 - 8856034131</p>
                         <p class="lg indent20"> +91 - 7709588893 </p>
                         <p class="lg indent20"> +91 - 7276366128</p>
                				<p class="lg"><span class="icon-close-envelope"></span> info@studentduniya.com</p>
              				</div>
              			</div>
            		</div>
          		</div>
        	</div>
      	</div>
    </section>
@endsection