@extends('layouts.dash')

@section('content')
<?php use App\Http\Controllers\Controller; ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Subscribers</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Subscribers List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Subscribers List</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th style="width: 10px">S.No</th>
                                    <th>Name</th>
                                    <th width="29%">Email</th>
                                    <th>Mobile</th>
                                    <th width="8%">Action</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
