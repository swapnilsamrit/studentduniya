<footer>
    <div class="container text-white padding30">
        <div class="col-xs-12 no-padding">
            <div class="col-xs-5 no-padding about-us">
                <div class="col-xs-12 margin20 no-padding">
                    <a href="index.php"><img class="logo" src="{{ asset('public/website/img/logo3.svg') }}" alt="Student-Duniya"></a>
                </div>
                <div class="col-xs-12">
                    <p class="sm text-justify">We are the engineering students, driving this website with our supporting team. It is founded by Pranay Morey, Saif Ali and Kunal Mahalle. It is a public website made for student welfare. There is more future plan for our website which are coming soon, if you got any idea regarding our website contact us.</p>
                </div>
            </div>
            <div class="col-sm-7 col-xs-12 no-padding">
                <div class="col-xs-12 no-padding">
                    <div class="col-xs-4 short-links footer-nav-links">
                        <h6>QUICK LINKS</h6>
                        <ul class="footer-links no-padding">
                            <li><a href="{{ route('webindex') }}">Home</a></li>
                            <li><a href="javascript:void(0);">Notes</a></li>
                            <li><a href="javascript:void(0);">Paper</a></li>
                            <li><a href="{{ route('about') }}">About</a></li>
                            <li><a href="{{ route('contact') }}">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-xs-12 short-links">
                        <h6>INFORMATION</h6>
                        <ul class="footer-links no-padding">
                            <li><a href="javascript:void(0);">About Team</a></li>
                            <li><a href="javascript:void(0);">Join Us</a></li>
                            <li><a href="javascript:void(0);">Ask a Question</a></li>
                            <li><a href="javascript:void(0);">Feedback</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 col-xs-12 short-links">
                        <h6>MORE</h6>
                        <ul class="footer-links no-padding">
                            <li><a href="javascript:void(0);">Upload</a></li>
                            <li><a href="javascript:void(0);">Sitemap</a></li>
                            <li><a href="javascript:void(0);">Locations</a></li>
                            <li><a href="javascript:void(0);">Policies</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid no-padding text-white footer-strip">
        <div class="container">
            <p class="lg col-sm-7 col-xs-12 no-padding">&copy; 2016 Copyright: studentduniya.com All rights reserved</p>
            <ul class="social-links col-sm-5 col-xs-12 text-right no-margin">
                <li><a href="https://www.facebook.com/rtmnustudents/"><span class="icon-facebook-logo-in-circular-button-outlined-social-symbol"></span></a></li>
                <li><a href="#"><span class="icon-social"></span></a></li>
                <li><a href="#"><span class="icon-linkedin"></span></a></li>
                <li><a href="#"><span class="icon-twitter-circular-button"></span></a></li>
            </ul>
        </div>
    </div>
</footer> 