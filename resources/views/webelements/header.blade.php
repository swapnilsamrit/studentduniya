<nav class="navbar-fixed-top nav-custom " id="header">
    <div class="container no-padding">   
        <div class="col-xs-12">
        <div class="col-md-3 col-xs-5 logo-section clearfix">
            <a href="index.php"><img class="logo" src="{{ asset('public/website/img/logo2.svg') }}" alt="Student-Duniya"></a>
        </div>
        <div class="navbar-header clearfix">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navigation col-md-offset-1 col-md-6 col-sm-8 col-xs-4 no-padding ">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav head-nav clearfix col-xs-12 no-padding
                text-white">
                    <li><a href="{{ route('webindex') }}">HOME</a></li>
                    <li><a href="{{ route('about') }}">ABOUT</a></li>
                    <li><a href="{{ route('contact') }}">CONTACT US</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-2 col-sm-4 no-padding login-section">

            <!-- <a href="#" class="btn btn-success navbar-btn nav-button">SIGN IN</a>
            <a href="#" class="btn btn-default navbar-btn nav-button trans-btn">LOGIN</a> -->
        </div>
        </div>
    </div>
</nav>