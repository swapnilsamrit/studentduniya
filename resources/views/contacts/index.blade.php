@extends('layouts.dash')

@section('content')
<?php use App\Http\Controllers\Controller; ?>
<div class="content-wrapper">
    <section class="content-header">
        <h1>Enquiries</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Enquiries List</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enquiries List</h3>
                    </div>
                    <div class="box-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <th style="width: 10px">S.No</th>
                                    <th>Name</th>
                                    <th width="29%">Email</th>
                                    <th>Mobile</th>
                                    <th width="8%">Action</th>
                                </tr>
                                @foreach($allcontactes as $contacte)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $contacte->fname }} {{ $contacte->lname }}</td>
                                        <td>{{ $contacte->email }}</td>
                                        <td>{{ $contacte->phone }}</td>
                                        <td>
                                            <!-- <a href="javascript:void(0)" data-item-id="" data-toggle="modal" data-target="#modal-default" claas="btn btn-success" id="modelbtn">view</a> -->
                                            <button type="button" onclick="getUserdata({{ json_encode($contacte) }})" claas="btn btn-success">
                                                <i class="fa fa-fw fa-eye"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach    
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    var userDataObj = @json($allcontactes);
    var userDataObj = @json($allcontactes, JSON_PRETTY_PRINT);
</script>
<!-- modal -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">
                    <span id="firstNameId"></span>
                    <span id="lastNameId"></span>
                </h4>
            </div>
            <div class="modal-body">
                <p><b>Eduction :</b>  <span id="eduNameId"></span></p>
                <p><b>School :</b>  <span id="schoolNameId"></span></p>
                <p><b>Message :</b>  <span id="messageNameId"></span></p>
            </div>
        </div>
    </div>
</div>
<!-- end modal -->
@endsection
