@extends('layouts.dash')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
        <h1>Role</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active"> Edit Role</li>
        </ol>
    </section>
    @php
        $reqlabel = '<sup class="text-danger">*</sup>';
        $permisionArr = json_decode($role->permissions, true);
    @endphp
    <section class="content">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Edit Roles</h3>
            </div>
            <form role="form" method="post" action="{{ route('role-update', ['role' => $role->id]) }}">
                @csrf
                <div class="box-body">
                    <div class="form-group col-md-4">
                        <label for="exampleInputName">Name</label>
                        <input type="text" class="form-control" id="nameId" placeholder="Enter role name" name="name" value="{{ old('name', $role->name) }}" required="" autofocus @if($role->id == 1) readonly @endif>
                        @if ($errors->has('name'))
                            <span class="text-danger" role="alert">
                                <small>{{ $errors->first('name') }}</small>
                            </span>
                        @endif
                    </div>    
                    <div class="col-md-12">
                        @foreach($modules as $module)
                            <div class="box box-default noborder">
                                <div class="box-body" style="padding-top: unset;">
                                    <h5><b>{{ $module->name }}</b></h5>
                                    @foreach($module->submodules->chunk(4) as $modchunk)
                                        <div class="row clearfix" style="margin-bottom:6px;">
                                            @foreach($modchunk as $submodule)
                                                <div class="col-md-3 subdiv">
                                                    <li class="list-group-item bg-blue text-white" style="padding: .25rem 0.75rem;">
                                                        <div class="checkbox" style="margin: unset;">
                                                            <label>
                                                                <input type="checkbox" class="parentCheckBox" onclick="checkAllChild(this);" value="">{{ $submodule->name }}
                                                            </label>
                                                        </div>
                                                    </li>
                                                    @if(!empty($submodule->methods) && count($submodule->methods) > 0)
                                                        <li class="list-group-item">
                                                            <ul class="list-group" style="margin: unset;">
                                                                @foreach($submodule->methods as $method)
                                                                    <li class="list-group-item" style="padding: .25rem 0.75rem;">
                                                                        <div class="checkbox" style="margin: unset;">
                                                                            <label>
                                                                                <input type="checkbox" class="childCheckBox" onclick="checkForParent(this);" name="checked[{{ $method->route_name }}]" value="" @if(array_key_exists($method->route_name,$permisionArr)) checked @endif> {{ $method->route_name }}
                                                                            </label>
                                                                        </div>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </li>
                                                    @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                    <div class="box-footer text-right">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <button type="reset" class="btn btn-default">Cancel</button>
                    </div>
            </form>
        </div>
    </section>
</div>
@endsection
