<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Config;

class Role extends Model
{
    protected $fillable = ['name','slug','permissions'];

    public function users()
    {
        return $this->belongsToMany('App\User', 'role_users');
    }

    public function getRolesList()
    {
        return Role::orderBy('name')->where('status', 1)->pluck('name','id');
    }

    public function getListOfAllRoles()
    {
        return Role::select(['id','name','permissions'])->where('status', 1)->orderBy('id', 'DESC')->paginate(Config::get('constant.datalength'));
    }

    public function saveRole(Request $request)
    {
        $saveResult = false;
        DB::beginTransaction();
        $permitdata = !empty($request->checked) ? $request->checked : [];
        foreach($permitdata as $key => $value)
        {
            $permitdata[$key] = true;
        }

        $data                   = $request->only('name');
        $data['slug']           = str_slug($data['name']);
        $data['permissions']    = json_encode($permitdata);
        $saveResult = Role::create($data);
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }

    public function updateRole(Role $role, Request $request)
    {
        $saveResult = false;
        DB::beginTransaction();
        $permitdata = !empty($request->checked) ? $request->checked : [];
        foreach ($permitdata as $key => $value) {
            $permitdata[$key] = true;
        }

        $data                   = $request->only('name');
        $data['slug']           = str_slug($data['name']);
        $data['permissions']    = json_encode($permitdata);

        $saveResult = $role->fill($data)->save();
        if($saveResult){
            DB::commit();
        }else{
            dd($saveResult);
            DB::rollBack();
        }
        return $saveResult;
    }

    public function hasAccess(array $permissions)
    {
        foreach ($permissions as $permission) {
            if($this->hasPermission($permission)){
                return true;
            }
        }
        return false;
    }

    protected function hasPermission(string $permission)
    {
        $permissions = json_decode($this->permissions,true);
        return $permissions[$permission] ?? false;
    }
}
