<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $table = 'modules';

    public function submodules()
    {
        return $this->hasMany('App\Module', 'parent');
    }

    public function methods(){
    	return $this->hasMany('App\Method');
    }

    public function getModuleAndMethod()
    {
        return Module::select(['id','name','parent'])
                    ->with(['submodules:id,name,parent','submodules.methods:id,module_id,route_name,published'])
                    ->where('parent',0)->get();
    }
}
