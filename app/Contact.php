<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Config;

class Contact extends Model
{
    protected $fillable = ['fname','lname','eduction','school','email','phone','message'];

    public function getListOfAllContacts()
    {
        return Contact::select(['id','fname','lname','eduction','school','email','phone','message'])->where('status', 1)->orderBy('id', 'DESC')->paginate(Config::get('constant.datalength'));
    }
}
