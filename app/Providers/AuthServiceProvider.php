<?php

namespace App\Providers;

use App\Policies\SystemPolicy;
use App\Policies\MasterPolicy;
use App\Policies\EnquiryPolicy;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        SystemPolicy::userPolicies();
        SystemPolicy::rolePolicies();
        MasterPolicy::universityPolicies();
        MasterPolicy::coursePolicies();
        MasterPolicy::questionPolicies();
        EnquiryPolicy::enquiryPolicies();
    }
}
