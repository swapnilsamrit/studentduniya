<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class EnquiryPolicy
{
    use HandlesAuthorization;

    public static function enquiryPolicies(){
        Gate::define('contacts-list', function ($user) {
            return $user->hasAccess(['contacts-list']);
        });
        Gate::define('subscribers-list', function ($user) {
            return $user->hasAccess(['subscribers-list']);
        });
    }
}
