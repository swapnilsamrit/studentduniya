<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class MasterPolicy
{
    use HandlesAuthorization;

    public static function universityPolicies(){
        Gate::define('universities-list', function ($user) {
            return $user->hasAccess(['universities-list']);
        });

        Gate::define('universities-add', function ($user) {
            return $user->hasAccess(['universities-add']);
        });

        Gate::define('universities-edit', function ($user) {
            return $user->hasAccess(['universities-edit']);
        });

        Gate::define('universities-delete', function ($user) {
            return $user->hasAccess(['universities-delete']);
        });
    }

    public static function coursePolicies(){
        Gate::define('courses-list', function ($user) {
            return $user->hasAccess(['courses-list']);
        });

        Gate::define('courses-add', function ($user) {
            return $user->hasAccess(['courses-add']);
        });

        Gate::define('courses-edit', function ($user) {
            return $user->hasAccess(['courses-edit']);
        });

        Gate::define('courses-delete', function ($user) {
            return $user->hasAccess(['courses-delete']);
        });
    }

    public static function questionPolicies(){
        Gate::define('questions-list', function ($user) {
            return $user->hasAccess(['questions-list']);
        });

        Gate::define('questions-add', function ($user) {
            return $user->hasAccess(['questions-add']);
        });

        Gate::define('questions-edit', function ($user) {
            return $user->hasAccess(['questions-edit']);
        });

        Gate::define('questions-delete', function ($user) {
            return $user->hasAccess(['questions-delete']);
        });
    }
}
