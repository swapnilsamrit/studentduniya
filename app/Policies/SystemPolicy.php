<?php

namespace App\Policies;

use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\HandlesAuthorization;

class SystemPolicy
{
    use HandlesAuthorization;

    public static function userPolicies(){
        Gate::define('user-list', function ($user) {
            return $user->hasAccess(['user-list']);
        });

        Gate::define('user-add', function ($user) {
            return $user->hasAccess(['user-add']);
        });

        Gate::define('user-edit', function ($user) {
            return $user->hasAccess(['user-edit']);
        });

        Gate::define('user-delete', function ($user) {
            return $user->hasAccess(['user-delete']);
        });
    }

    public static function rolePolicies(){
        Gate::define('role-list', function ($user) {
            return $user->hasAccess(['role-list']);
        });

        Gate::define('role-add', function ($user) {
            return $user->hasAccess(['role-add']);
        });

        Gate::define('role-edit', function ($user) {
            return $user->hasAccess(['role-edit']);
        });

        Gate::define('role-delete', function ($user) {
            return $user->hasAccess(['role-delete']);
        });

        Gate::define('role-permissions', function ($user) {
            return $user->hasAccess(['role-permissions']);
        });
    }
}
