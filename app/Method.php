<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Method extends Model
{
    protected $table = 'methods';

    public function modules(){
        return $this->belongsTo('App\Module', 'module_id');
    }
}
