<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Config;

class Question extends Model
{
    protected $fillable = ['university_id','course_id','year','session','subject_name','file_url'];

    public function courses()
    {
        return $this->belongsTo('App\Course', 'course_id');
    }

    public function university()
    {
        return $this->belongsTo('App\University', 'university_id');
    }

    public function getListOfAllQuestions()
    {
        return Question::where('status', 1)->with('courses','university')->orderBy('id', 'DESC')->paginate(Config::get('constant.datalength'));
    }

    public function saveQuestion(Request $request)
    {
        $saveResult = false;
        DB::beginTransaction();

        $data = $request->only('university_id','course_id','year','session','subject_name');
        $data['file_url'] = !empty($request->file('file_url')) ? $this->fileupload($request->file('file_url'), 'papers') : null;
        $saveResult = Question::create($data);
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }

    protected function fileupload($selectedfile, $folder){
        $filepath = null;
        $pdf = $selectedfile;
        $input['file_url'] = time().'.'.$pdf->getClientOriginalExtension();
        $destinationPath = public_path('/'.$folder);
        if($pdf->move($destinationPath, $input['file_url'])){
            $filepath = url('/public/'.$folder)."/".$input['file_url'];
        }
        return $filepath;
    }
}
