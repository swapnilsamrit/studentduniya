<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Config;

class University extends Model
{
    protected $fillable = ['name','description'];

    public function courses()
    {
        return $this->hasMany('App\Course', 'university_id');
    }
    
    public function getUniversitiesList()
    {
        return University::where('status', 1)->select('id','name')->get();
    }

    public function getListOfAllUniversities()
    {
        return University::select(['id','name','description'])->where('status', 1)->orderBy('id', 'DESC')->paginate(Config::get('constant.datalength'));
    }

    public function saveUniversity(Request $request)
    {
        $saveResult = false;
        DB::beginTransaction();

        $data = $request->only('name','description');
        $saveResult = University::create($data);
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }

    public function updateUniversity(University $university, Request $request)
    {
        $saveResult = false;
        DB::beginTransaction();
        $data = $request->only('name','description');
        $saveResult = $university->fill($data)->save();
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }
}
