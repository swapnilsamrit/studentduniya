<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use DB;
use Config;

class Course extends Model
{
    protected $fillable = ['university_id','semester','name'];

    public function university()
    {
        return $this->belongsTo('App\University');
    }

    public function question()
    {
        return $this->hasOne('App\Question');
    }

    public function getCoursesList()
    {
        return Course::where('status', 1)->select('id','semester','name')->get();
    }

    public function getListOfAllCourses()
    {
        return Course::select(['id','university_id','semester','name'])->where('status', 1)->orderBy('id', 'DESC')->paginate(Config::get('constant.datalength'));
    }

    public function saveCourse(Request $request)
    {
        $saveResult = false;
        DB::beginTransaction();

        $data = $request->only('university_id','semester','name');
        $saveResult = Course::create($data);
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }

    public function updateCourse(Course $course, Request $request)
    {
        $saveResult = false;
        DB::beginTransaction();
        $data = $request->only('university_id','semester','name');
        $saveResult = $course->fill($data)->save();
        if($saveResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $saveResult;
    }
}
