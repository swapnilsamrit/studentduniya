<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redirect;
use Session;
use DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function my_simple_crypt( $string, $action = 'e' ) {
        // you may change these values to your own
        $secret_key = 'myVibration_secret_key';
        $secret_iv = 'myVibration_secret_iv';

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash( 'sha256', $secret_key );
        $iv = substr( hash( 'sha256', $secret_iv ), 0, 16 );

        if( $action == 'e' ) {
            $output = base64_encode( openssl_encrypt( $string, $encrypt_method, $key, 0, $iv ) );
        }
        else if( $action == 'd' ){
            $output = openssl_decrypt( base64_decode( $string ), $encrypt_method, $key, 0, $iv );
        }
        return $output;
    }

    public function delete($id, $tablename){
        try {
            if(!$this->isPresentOnAnotherTable($id, $tablename)){
                if(DB::table($tablename)->where('id', $id)->update(['status' => 0])){
                    Session::flash('message', 'Deleted Successfully');
                    Session::flash('alert-class', 'success');
                    return Redirect::back();
                }else{
                    Session::flash('message', 'Failed To Delete Try Again!!!');
                    Session::flash('alert-class', 'danger');
                    return Redirect::back();
                }
            }else{
                Session::flash('warning', 'Cannot Delete this entity, It is already in used. !!!');
                return Redirect::back();
            }
        }catch (\Exception $e) {
           return Redirect::back()->with('warning', $e->getMessage());
        }
    }

    protected function isPresentOnAnotherTable($id, $tablename){
        $status = false;
        switch ($tablename) {
            case 'roles':
                $status = DB::table('role_users')->where(['role_id' => $id])->exists();
                return $status;
            break;
            default:
               return $status;
            break;
        }
    }
}
