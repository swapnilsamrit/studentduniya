<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\University;
use App\Course;
use Config;

class HomeController extends Controller
{
    public $exception;
    public $contact;
    public $university;
    public $course;

    public function __construct(Contact $contact, University $university, Course $course)
    {
        $this->exception = 'home';
        $this->middleware('auth');
        $this->contact     = $contact;
        $this->university     = $university;
        $this->course     = $course;
    }
    
    public function index()
    {
        $countOfUniversities = $this->university->getListOfAllUniversities()->count();
        $countOfcourses = $this->course->getListOfAllCourses()->count();
        return view('home', compact('countOfUniversities', 'countOfcourses'));
    }

    public function contact()
    {
        try{
            $allcontactes = $this->contact->getListOfAllContacts();
            return view('contacts.index', compact('allcontactes'));
        }catch(\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    public function subscriber()
    {
        return view('subscribers.index');
    }
}
