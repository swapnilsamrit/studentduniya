<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Session;
use App\University;
use App\Question;
use App\Course;

class QuestionController extends Controller
{
    public $exception;
    public $question;
    public $university;
    public $course;

    public function __construct(Question $question,University $university,Course $course)
    {
        $this->exception = 'home';
        $this->question = $question;
        $this->university = $university;
        $this->course = $course;
        $this->middleware('auth');
    }

    public function index()
    {
        try{
            $allquestions = $this->question->getListOfAllQuestions();
            return view('questions.index', compact('allquestions'));
        }catch(\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }
    
    public function add(Question $question, Request $request)
    {
        try{
            if($request->isMethod('post')){
                $validator = $this->getValidateQuestion($this->question, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                if($this->question->saveQuestion($request)){
                    Session::flash('message', 'Question Upload Successfully Created !');
                    Session::flash('alert-class', 'success');
                    return redirect()->route('questions-list');
                }else{
                    Session::flash('message', 'Unable to Upload Question..! Try Again');
                    Session::flash('alert-class', 'danger');
                }
            }
            $universities = $this->university->getUniversitiesList();
            $courses = $this->course->getCoursesList();
            return view('questions.add', compact('universities','courses'));
        }catch(\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    public function downloadFile()
    {
    	$myFile = public_path("dummy_pdf.pdf");
    	$headers = ['Content-Type: application/pdf'];
    	$newName = 'itsolutionstuff-pdf-file-'.time().'.pdf';

    	return response()->download($myFile, $newName, $headers);
    }

    protected function getValidateQuestion(Question $question, $data){
        $rules = [
            'university_id' => 'required',
            'course_id' => 'required',
            'year' => 'required',
            'session' => 'required',
            'subject_name' => 'required',
            'file_url' => 'required',
        ];
        $errmsg = [
            'university_id.required' => "Please select university",
            'course_id.required' => "Please select course",
            'year.required' => "Please enter year",
            'session.required' => "Please enter session",
            'subject_name.required' => "Please enter subject name",
            'file_url.required' => "Please upload the file",
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
