<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use App\Role;
use App\User;
use Session;

class UserController extends Controller
{   
    public $exception;
    public $role;
    public $user;

    public function __construct(Role $role, User $user)
    {
        $this->exception = 'home';
        $this->user = $user;    
        $this->role = $role;    
        $this->middleware('auth');
    }

    public function index(Request $request)
    { 
        try{
            $userlist = $this->user->getUsersList($request);
            $roles = $this->role->getRolesList();
            return view('users.index', compact('userlist','roles'));
        }catch(\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    public function add()
    {
        try{
            $roles = $this->role->getRolesList();
            return view('users.add', compact('roles'));
        }catch(\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    public function edit($id, Request $request)
    {
        try{
            $decid = $this->my_simple_crypt($id, "d");
            $user = User::findOrFail($decid);
            if($request->isMethod('post')){
                $validator = $this->validatorUser($request->all());
                if($validator->fails()){
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                if($this->user->updateUser($user, $request)){
                    Session::flash('message', 'User Successfully Updated !');
                    Session::flash('alert-class', 'success');
                    return redirect()->route('user-list');
                }else{
                    Session::flash('message', 'Unable to Update User..! Try Again');
                    Session::flash('alert-class', 'danger');
                    return back();
                }
            }
            $roles = $this->role->getRolesList();
            return view('users.edit', compact('roles','user'));
        }catch  (\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    protected function validatorUser($data)
    { 
        $rules = [
            'name'      => 'required|string|max:255',
            'email'     => 'required|string|email|max:255',
            'mobile'    => 'required|numeric|digits:10',
            'password'  => 'nullable|string|min:6|confirmed',
            'role_id'   => 'required'
        ];
        $errmsgs = [
            'name.required'       => 'Name is required.',
            'email.required'      => 'Email Address is required.',
            'role_id.required'    => 'Please Select Role.',
            'mobile.required'     => 'Mobile no. is required.',
            'email.email'         => 'Please Enter valid email address.',
            'mobile.numeric'      => 'Please Enter only numbers.',
            'mobile.digits'       => 'Please Enter only 10 digits.',
        ];
        return Validator::make($data, $rules, $errmsgs);
    }
}
