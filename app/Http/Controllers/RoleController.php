<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Session;
use App\Module;
use App\Role;

class RoleController extends Controller
{
    public $exception;
    public $module;
    public $role;

    public function __construct(Module $module, Role $role)
    {
        $this->exception = 'home';
        $this->module   = $module;
        $this->role     = $role;
        $this->middleware('auth');
    }

    public function index()
    {
        try{
            $allroles = $this->role->getListOfAllRoles();
            return view('roles.index', compact('allroles'));
        }catch(\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    public function add(Role $role, Request $request)
    {
        try{
            if($request->isMethod('post')){
                if($this->role->saveRole($request)){
                    Session::flash('message', 'Role Successfully Created !');
                    Session::flash('alert-class', 'success');
                    return redirect()->route('role-list');
                }else{
                    Session::flash('message', 'Unable to Save Role..! Try Again');
                    Session::flash('alert-class', 'danger');
                }
            }
            $modules = $this->module->getModuleAndMethod();
            // dd($modules);
            return view('roles.add', compact('modules'));
        }catch(\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    public function edit($encid)
    {
        try {
            $decid = $this->my_simple_crypt($encid, "d");
            $role = Role::findOrFail($decid);
            $modules = $this->module->getModuleAndMethod();
            // dd($modules);
            return view('roles.edit', compact('role','modules'));
        }catch (\Exception $e) {
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    public function update(Role $role, Request $request)
    {
        try{
            if($this->role->updateRole($role, $request)){
                Session::flash('message', 'Role Successfully Update !');
                Session::flash('alert-class', 'success');
                return redirect()->route('role-list');
            }else{
                Session::flash('message', 'Unable to Update Role..! Try Again');
                Session::flash('alert-class', 'danger');
                return back();
            }
        }catch(\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }
}
