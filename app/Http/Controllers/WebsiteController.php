<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class WebsiteController extends Controller
{
    public function index()
    {
        return view('website.index');
    }

    public function about()
    {
        return view('website.about');
    }

    public function contact(Request $request)
    {
        if($request->isMethod('post')){
            $input = $request->all();
            $contact = new Contact;
            $contact->fname = $input['fname'];
            $contact->lname = $input['lname'];
            $contact->eduction = $input['eduction'];
            $contact->school = $input['school'];
            $contact->email = $input['email'];
            $contact->phone = $input['phone'];
            $contact->message = $input['message'];
            $contact->save();
            return redirect('contact')->with('success', 'Enquiry send Successfully!');
        }
        return view('website.contact');
    }

    public function questionpaper()
    {
        return view('website.questionpaper');
    }
}
