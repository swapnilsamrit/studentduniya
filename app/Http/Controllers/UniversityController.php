<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Session;
use App\University;
use Gate;

class UniversityController extends Controller
{
    public $exception;
    public $university;

    public function __construct(University $university)
    {
        $this->exception = 'home';
        $this->university = $university;
        $this->middleware('auth');
    }

    public function index()
    {
        try{
            $alluniversities = $this->university->getListOfAllUniversities();
            return view('universities.index', compact('alluniversities'));
        }catch(\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    public function add(University $university, Request $request)
    {
        try{
            if($request->isMethod('post')){
                $validator = $this->getValidateUniversity($this->university, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                if($this->university->saveUniversity($request)){
                    Session::flash('message', 'University Successfully Created !');
                    Session::flash('alert-class', 'success');
                    return redirect()->route('universities-list');
                }else{
                    Session::flash('message', 'Unable to Save University..! Try Again');
                    Session::flash('alert-class', 'danger');
                }
            }
            return view('universities.add');
        }catch(\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }
    
    public function edit($encid, Request $request)
    {
        try {
            $decid = $this->my_simple_crypt($encid, "d");
            $university = University::findOrFail($decid);
            if($request->isMethod('post')){
                $validator = $this->getValidateUniversity($university, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                if($this->university->updateUniversity($university, $request)){
                    Session::flash('message', 'University Successfully Updated !');
                    Session::flash('alert-class', 'success');
                    return redirect()->route('universities-list');
                }else{
                    Session::flash('message', 'Unable to Update University..! Try Again');
                    Session::flash('alert-class', 'danger');
                    return back();
                }
            }
            return view('universities.edit', compact('university'));
        }catch (\Exception $e) {
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    protected function getValidateUniversity(University $university, $data){
        $rules = [
            'name' => 'required',
        ];
        $errmsg = [
            'name.required' => "Name is required",
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
