<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Services\PayUService\Exception;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Session;
use App\University;
use App\Course;

class CourseController extends Controller
{
    public $exception;
    public $university;
    public $course;

    public function __construct(University $university, Course $course)
    {
        $this->exception = 'home';
        $this->university = $university;
        $this->course = $course;
        $this->middleware('auth');
    }

    public function index()
    {   
        try{
            $allcourses = $this->course->getListOfAllCourses();
            return view('courses.index', compact('allcourses'));
        }catch(\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    public function add(Course $course, Request $request)
    {   
        try{
            if($request->isMethod('post')){
                $validator = $this->getValidateCourse($this->course, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                if($this->course->saveCourse($request)){
                    Session::flash('message', 'Course Successfully Created !');
                    Session::flash('alert-class', 'success');
                    return redirect()->route('courses-list');
                }else{
                    Session::flash('message', 'Unable to Save Course..! Try Again');
                    Session::flash('alert-class', 'danger');
                }
            }
            $universities = $this->university->getUniversitiesList();
            return view('courses.add', compact('universities'));
        }catch(\Exception $e){
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    public function edit($encid, Request $request)
    {
        try {
            $decid = $this->my_simple_crypt($encid, "d");
            $course = Course::findOrFail($decid);
            $universities = $this->university->getUniversitiesList();
            if($request->isMethod('post')){
                $validator = $this->getValidateCourse($course, $request->all());
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                if($this->course->updateCourse($course, $request)){
                    Session::flash('message', 'Course Successfully Updated !');
                    Session::flash('alert-class', 'success');
                    return redirect()->route('courses-list');
                }else{
                    Session::flash('message', 'Unable to Update Course..! Try Again');
                    Session::flash('alert-class', 'danger');
                    return back();
                }
            }
            return view('courses.edit', compact('course','universities'));
        }catch (\Exception $e) {
            return redirect()->route($this->exception)->with('warning', $e->getMessage());
        }
    }

    protected function getValidateCourse(Course $course, $data){
        $rules = [
            'university_id' => 'required',
            'semester' => 'required',
            'name' => 'required',
        ];
        $errmsg = [
            'university_id.required' => "Please select university",
            'semester.required' => "Please select semester",
            'name.required' => "Please enter course name",
        ];
        return Validator::make($data, $rules, $errmsg);
    }
}
