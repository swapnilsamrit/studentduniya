<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showRegistrationForm()
    {
        return redirect()->route('home');
    }

    public function register(Request $request)
    {
        if($request->isMethod('post')){
            $this->validator($request->all())->validate();
            $user = $this->create($request->all());
            if(!empty($user)){
                Session::flash('message', 'User Successfully Created !');
                Session::flash('alert-class', 'success');
                return redirect()->route('userList');

            }else{
                Session::flash('message', 'Unable to Save User..! Try Again');
                Session::flash('alert-class', 'danger');
            }
            return redirect()->back();
        }
    }
    
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'mobile'    => ['required', 'regex:/^[6-9]\d{9}$/', 'digits:10'],
            'is_active'   => ['required'],
        ]);
    }
    
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'mobile' => $data['mobile'],
            'is_active' => !empty($data['is_active']) ? true : false,
        ]);
        $user->roles()->attach($data['role_id']);
        return $user;
    }
}
