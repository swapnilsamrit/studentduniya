<?php

namespace App;

use Illuminate\Http\Request;
use DB;
use Config;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'mobile', 'is_active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getUsersList(Request $request)
    {
        $userslist = User::select(['users.id','name','email','mobile','is_active'])->where('status', '1')
                     ->orderBy('updated_at', 'desc')
                     ->paginate(Config::get('constant.datalength'));
        return $userslist;
    }
    
    public function updateUser(User $user, Request $request){
        $updateResult = false;
        DB::beginTransaction();
        $data = $request->only('name','email','mobile');
        $data['is_active'] = !empty($request->is_active) ? true : false;
        $data['password'] = !empty($request->password) ? Hash::make($request->password) : $user->password;
        $user->roles()->sync([$request->only('role_id')]);
        $updateResult = $user->fill($data)->save();
        if($updateResult){
            DB::commit();
        }else{
            DB::rollBack();
        }
        return $updateResult;
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'role_users');
    }

    public function hasAccess(array $permissions)
    {
        foreach ($this->roles as $role) {
            if($role->hasAccess($permissions)){
                return true;
            }
        }
        return false;
    }

    public function inRole($roleSlug)
    {
        return $this->roles()->where('slug', $roleSlug)->count() == 1;
    }
    

    
}
