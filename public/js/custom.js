$(function(){
    $('#universityId').change(function(){
        $('#semesterId').prop("disabled", false);
    });

    $('#selectUniversityId').change(function () {
        $('#selectCourseId').prop("disabled", false);
    });

    $('#selectCourseId').change(function () {
        $('#selectYearId').prop("disabled", false);
    });
    $('#selectYearId').change(function () {
        $('#selectSessionId').prop("disabled", false);
    });
});

// enquiry modal
function getUserdata(userObj){
    $("#modal-default").modal('show');
    console.log(userObj);
    $("#firstNameId").text(userObj.fname);
    $("#lastNameId").text(userObj.lname);
    $("#eduNameId").text(userObj.eduction);
    $("#schoolNameId").text(userObj.school);
    $("#messageNameId").text(userObj.message);
}