<?php

Route::group(['middleware' => 'web'], function () {
    Route::get('/', 'WebsiteController@index')->name('webindex');
    Route::get('/about', 'WebsiteController@about')->name('about');
    Route::match(['get','post'], '/contact',  'WebsiteController@contact')->name('contact');
    Route::get('/questionpaper',  'WebsiteController@questionpaper')->name('questionPaper');

    // Dashboard routes
    Auth::routes();
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/home', 'HomeController@index')->name('home');
    
        //system setting
        Route::get('/user-list', 'UserController@index')->name('user-list')->middleware('can:user-list');
        Route::get('/user-add', 'UserController@add')->name('user-add')->middleware('can:user-add');
        Route::match(['get','post'], '/user-edit/{user}', 'UserController@edit')->name('user-edit')->middleware('can:user-edit');
    
        // role routes
        Route::get('/role-list', 'RoleController@index')->name('role-list')->middleware('can:role-list');
        Route::match(['get','post'], '/role-add', 'RoleController@add')->name('role-add')->middleware('can:role-add');
        Route::get('/role-edit/{role}', 'RoleController@edit')->name('role-edit')->middleware('can:role-edit');
        Route::post('/role-edit/{role}', 'RoleController@update')->name('role-update');
    
        // universities routes
        Route::get('/universities-list', 'UniversityController@index')->name('universities-list')->middleware('can:universities-list');
        Route::match(['get','post'], '/universities-add', 'UniversityController@add')->name('universities-add')->middleware('can:universities-add');
        Route::match(['get','post'], '/universities-edit/{universities}', 'UniversityController@edit')->name('universities-edit')->middleware('can:universities-edit');
    
        // courses routes
        Route::get('/courses-list', 'CourseController@index')->name('courses-list')->middleware('can:courses-list');
        Route::match(['get','post'], '/courses-add', 'CourseController@add')->name('courses-add')->middleware('can:courses-add');
        Route::match(['get','post'], '/courses-edit/{course}', 'CourseController@edit')->name('courses-edit')->middleware('can:courses-edit');
    
        // questions routes
        Route::get('/questions-list', 'QuestionController@index')->name('questions-list')->middleware('can:questions-list');
        Route::match(['get','post'], '/questions-add', 'QuestionController@add')->name('questions-add')->middleware('can:questions-add');

        // enquiries routes
        Route::get('/contacts-list', 'HomeController@contact')->name('contacts-list')->middleware('can:contacts-list');
        Route::get('/subscribers-list', 'HomeController@subscriber')->name('subscribers-list')->middleware('can:subscribers-list');
        
    
        //delete route
        Route::get('/dashboard/{id}/{model}', 'Controller@delete')->name('delete.submit');
    });
});
